// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	vscode.window.setStatusBarMessage('你好，朗新一诺!!！');

	require('./hover')(context); // 悬停提示
	require('./project-controller')(context); //左侧控制台
	require('./create-hooks')(context); //左侧控制台
	
}

// This method is called when your extension is deactivated
export function deactivate() {}
