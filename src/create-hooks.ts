import * as vscode from 'vscode';
const path = require("path");


async function createTemplate (uri: { path: string; }, fileNmae: string) {

  const folderUri = (vscode.workspace.workspaceFolders as any)[0].uri;

  /**
   * 读取模板
   */
  const templateurl = path.join(__filename, '..', '..', 'template', fileNmae);
  const templateUri = folderUri.with({ path: path.posix.join(templateurl) });
  const readData = await vscode.workspace.fs.readFile(templateUri);
  const writeData = Buffer.from(readData);

  /**
   * 在当前选中路径下创建模板
   */
  let newFile:any = uri.path.split("/");
  newFile[newFile.length - 1] = fileNmae;
  newFile = newFile.join("/");
  const fileUri = folderUri.with({ path: path.posix.join(newFile) });

  await vscode.workspace.fs.writeFile(fileUri, writeData);

}


module.exports = function (context: { subscriptions: vscode.Disposable[]; }) {


  context.subscriptions.push(vscode.commands.registerCommand('extension.useForms', async (uri) => {

    createTemplate(uri, "useForms.ts");

  }));

};