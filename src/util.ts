const fs = require('fs');
const os = require('os');
const path = require('path');
const vscode = require('vscode');
const exec = require('child_process').exec;

const util = {
  /**启动项目 */
  runProject (item: { name: any; cmd: any; }) {
    let terminalA = vscode.window.createTerminal({ name: item.name });
    terminalA.show(true);
    terminalA.sendText(`${item.cmd}`); //输入命令
  },
  /**
   * 获取当前所在工程根目录，有3种使用方法：<br>
   * getProjectPath(uri) uri 表示工程内某个文件的路径<br>
   * getProjectPath(document) document 表示当前被打开的文件document对象<br>
   * getProjectPath() 会自动从 activeTextEditor 拿document对象，如果没有拿到则报错
   * @param {*} document 
   */
  getProjectPath (document: { uri: any; }) {

    if (!document) {
      document = vscode.window.activeTextEditor ? vscode.window.activeTextEditor.document : null;
    }
    if (!document) {
      this.showError('当前激活的编辑器不是文件或者没有文件被打开！');
      return '';
    }
    const currentFile = (document.uri ? document.uri : document).fsPath;
    let projectPath = null;

    let workspaceFolder = vscode.workspace.workspaceFolders.map((item: { uri: { path: any; }; }) => item.uri.path);

    // this.showInfo('vscode.workspace！' + workspaceFolder);

    projectPath = workspaceFolder;
    if (!projectPath) {
      this.showError('获取工程根路径异常！');
      return '';
    }
    return projectPath;

  },
  // /**
  //  * 获取当前工程名
  //  */
  // getProjectName: function (projectPath) {
  //   return path.basename(projectPath);
  // },
  /**
 * 将一个单词首字母大写并返回
 * @param {*} word 某个字符串
 */
  upperFirstLetter: function (word: any) {
    return (word || '').replace(/^\w/, (m: string) => m.toUpperCase());
  },
  /**
   * 将一个单词首字母转小写并返回
   * @param {*} word 某个字符串
   */
  lowerFirstLeter: function (word: any) {
    return (word || '').replace(/^\w/, (m: string) => m.toLowerCase());
  },
  /**
   * 全局日志开关，发布时可以注释掉日志输出
   */
  log: function (...args: any) {
    console.log(...args);
  },
  /**
   * 全局日志开关，发布时可以注释掉日志输出
   */
  error: function (...args: any) {
    console.error(...args);
  },
  /**
   * 弹出错误信息
   */
  showError: function (info: any) {
    vscode.window.showErrorMessage(info);
  },
  /**
   * 弹出提示信息
   */
  showInfo: function (info: any) {
    vscode.window.showInformationMessage(info);
  },
  /**
   * 获取某个扩展文件绝对路径
   * @param context 上下文
   * @param relativePath 扩展中某个文件相对于根目录的路径，如 images/test.jpg
   */
  getExtensionFileAbsolutePath: function (context: { extensionPath: any; }, relativePath: any) {
    return path.join(context.extensionPath, relativePath);
  },
  /**
   * 获取某个扩展文件相对于webview需要的一种特殊路径格式
   * 形如：vscode-resource:/Users/toonces/projects/vscode-cat-coding/media/cat.gif
   * @param context 上下文
   * @param relativePath 扩展中某个文件相对于根目录的路径，如 images/test.jpg
   */
  getExtensionFileVscodeResource: function (context: { extensionPath: any; }, relativePath: any) {
    const diskPath = vscode.Uri.file(path.join(context.extensionPath, relativePath));
    return diskPath.with({ scheme: 'vscode-resource' }).toString();
  },
  /**
 * 获取某个字符串在文件里第一次出现位置的范围，
 */
  getStrRangeInFile: function (filePath: any, str: string | any[]) {
    var pos = this.findStrInFile(filePath, str as string);
    return new vscode.Range(new vscode.Position(pos.row, pos.col), new vscode.Position(pos.row, pos.col + str.length));
  },
  /**
 * 从某个文件里面查找某个字符串，返回第一个匹配处的行与列，未找到返回第一行第一列
 * @param filePath 要查找的文件
 * @param reg 正则对象，最好不要带g，也可以是字符串
 */
  findStrInFile: function (filePath: any, reg: string | RegExp) {
    const content = fs.readFileSync(filePath, 'utf-8');
    reg = typeof reg === 'string' ? new RegExp(reg, 'm') : reg;
    // 没找到直接返回
    if (content.search(reg) < 0) { return { row: 0, col: 0 }; }
    const rows = content.split(os.EOL);
    // 分行查找只为了拿到行
    for (let i = 0; i < rows.length; i++) {
      let col = rows[i].search(reg);
      if (col >= 0) {
        return { row: i, col };
      }
    }
    return { row: 0, col: 0 };
  },
  /**
 * 使用默认浏览器中打开某个URL
 */
  openUrlInBrowser: function (url: any) {
    // 旧版的使用这个api打开
    // vscode.commands.executeCommand('vscode.open', vscode.Uri.parse('https://example.com'))
    vscode.env.openExternal(vscode.Uri.parse(url));
    // exec(`open '${url}'`); 作废的方法
  },
  /**
   * 读取webview配置并写入sky.config.json配置当中
   */
  openFileInSkyConfig: function (path: any, text: string | any[], config: { writer: string; environment: string; }) {
    let options = {};
    if (text) {
      const selection = this.getStrRangeInFile(path, text);
      options = { selection };
    }
    vscode.workspace.openTextDocument(vscode.Uri.file(path)).then((doc: any) => {
      vscode.window.showTextDocument(doc, options).then((editor: { document: { getText: () => any; save: () => void; }; edit: (arg0: (editBuilder: any) => void) => void; }) => {

        // 从开始到结束，全量替换
        const end = new vscode.Position(vscode.window.activeTextEditor.document.lineCount + 1, 0);
        let text = editor.document.getText();

        const toObject = JSON.parse(text);
        toObject.name = config.writer || '苏志冬';
        toObject.environment = config.environment || "dev";

        text = JSON.stringify(toObject);

        editor.edit(editBuilder => {
          editBuilder.replace(new vscode.Range(new vscode.Position(0, 0), end), text);
        });
        // vscode.window.activeTextEditor.document.save();
        editor.document.save();

      });
    });

  },
  /**
* 在vscode中打开某个文件
* @param {*} path 文件绝对路径
* @param {*} text 可选，如果不为空，则选中第一处匹配的对应文字
*/
  openFileInVscode: function (path: any, text: string | any[]) {
    let options = {
      // 选中第23行第19列到第33行第17列
      // selection: new vscode.Range(new vscode.Position(22, 8), new vscode.Position(23, 16)),
      // 是否预览，默认true，预览的意思是下次再打开文件是否会替换当前文件
      // preview: false,
      // 显示在第二个编辑器
      // viewColumn: vscode.ViewColumn.Two
    };
    if (text) {
      const selection = this.getStrRangeInFile(path, text);
      options = { selection };
    }
    vscode.workspace.openTextDocument(vscode.Uri.file(path)).then((doc: any) => {
      vscode.window.showTextDocument(doc, options).then((editor: { selection: any; }) => {

        editor.selection = new vscode.Range(new vscode.Position(22, 8), new vscode.Position(23, 16));

        // const editor = vscode.window.activeTextEditor;  此处不需要从系统中获取
        // const document = editor.document;
        // const selection = editor.selection;  //当前选中的
        // const word = document.getText(selection);
        // const reversed = word.split('').reverse().join('');


        // 从开始到结束，全量替换
        // const end = new vscode.Position(vscode.window.activeTextEditor.document.lineCount + 1, 0);
        // const text = '新替换的内容';
        // editor.edit(editBuilder => {
        //   editBuilder.replace(new vscode.Range(new vscode.Position(0, 0), end), text);
        // });




        // 获取 vscode.TextEditorEdit对象， 然后进行字符处理
        // editor.edit(editorEdit => {
        //   // 这里可以做以下操作: 删除, 插入, 替换, 设置换行符
        //   // 以插入字符串为例: "Hello Word\r\n"
        //   editorEdit.insert(new vscode.Position(0, 0), "Hello Suzhidong\r\n");
        //   //替换name为myname
        //   editorEdit.replace(new vscode.Range(new vscode.Position(1, 3), new vscode.Position(1, 7)), "myname");
        //   //删除版本一行信息
        //   editorEdit.delete(new vscode.Range(new vscode.Position(2, 0), new vscode.Position(2, 999)));

        // }).then(isSuccess => {
        //   if (isSuccess) {
        //     console.log("Edit successed");
        //   } else {
        //     console.log("Edit failed");
        //   }
        // }, err => {
        //   console.error("Edit error, " + err);
        // });

      });
    });

  },

 async initConfigJson(){
    const writeStr = JSON.stringify({ cms: {}, proxy: {} });
    const writeData = Buffer.from(writeStr, 'utf8');
    const folderUri = vscode.workspace.workspaceFolders[0].uri;
    const fileUri = folderUri.with({ path: path.posix.join(folderUri.path, 'longnows.config.json') });
    try {
      const stat = await vscode.workspace.fs.stat(fileUri);
      util.showInfo("longnows.config.jsony已存在");
    } catch (error) {
      util.showInfo("longnows.config.json不存在");
      await vscode.workspace.fs.writeFile(fileUri, writeData);
    } finally {
      const readData = await vscode.workspace.fs.readFile(fileUri);
      const readStr = Buffer.from(readData).toString('utf8');
      const readObject = JSON.parse(readStr);
    }

  }
};
module.exports = util;