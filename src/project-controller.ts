import { TreeDataProvider, commands, Event, window, TreeItem, TreeItemCollapsibleState, ProviderResult } from "vscode";
const path = require("path");
const tools = require("./util");

export class DataProvider implements TreeDataProvider<DataItem> {
  onDidChangeTreeData?: Event<DataItem | null | undefined> | undefined;
  data: DataItem[];

  constructor() {
    this.data = [
      new DataItem('安装依赖'),
      new DataItem('启动开发环境'),
      new DataItem('构建生产环境')
    ];
  }

  getTreeItem(element: DataItem): TreeItem | Thenable<TreeItem> {
    return element;
  }

  getChildren(element?: DataItem | undefined): ProviderResult<DataItem[]> {
    if (element === undefined) {
      return this.data;
    }
    return element.children;
  }
}


class DataItem extends TreeItem {
  public children: DataItem[] | undefined;

  constructor(label: string, children?: DataItem[] | undefined) {
    super(label, children === undefined ? TreeItemCollapsibleState.None : TreeItemCollapsibleState.Collapsed);
    this.children = children;
  }

  iconChoose(){
   let icon = null;
   switch (this.label) {
     case "安装依赖":
       icon="install.svg";
       break;
    case "启动开发环境":
       icon="dev.svg";
      break;
    case "构建生产环境":
      icon="pro.svg";
      break;
     default:
       break;
   }
   return icon;
  }

  iconPath = {
		light: path.join(__filename, '..', '..','images', this.iconChoose()),
		dark: path.join(__filename, '..','..', 'images', this.iconChoose())
	};
}

module.exports = function (context: any) {


  const provider = new DataProvider();
  // 数据注册
  window.registerTreeDataProvider('longnows', provider);


  context.subscriptions.push(commands.registerCommand('longnows.editEntry', (item: DataItem) => {
    switch (item['label']) {
      case "安装依赖":
        //初始化工作
        tools.initConfigJson();
        tools.runProject({
          name:"安装依赖",
          cmd:"longnows i"
        });
        break;
      case "启动开发环境":
        tools.runProject({
          name:"启动开发环境",
          cmd:"longnows r"
        });
        break;
      case "构建生产环境":
        tools.runProject({
          name:"构建生产环境",
          cmd:"longnows build"
        });
        break;
      default:
        break;
    }

  }));

};

