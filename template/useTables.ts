import { ref } from 'vue'

// 状态、追溯号、物料编码、紧急程度、项目编号、项目名称、项目阶段、项目客户、项目采购经理、申购单号、申购单审批完结日期、商品名称、规格/型号、
// 采购业务员、用途、设备位号、子项号、申购人、需求数量、已购数量、成交单位、交货周期/天、需求接单日期、需求派单日期、要求合同签订日期、
// 预计合同签订日期、预算金额（万元)、要求返资料日期、预计返资料日期、项目要求集港日期、要求海外到货日期、采购类型、是否第三方、是否到港、
// 到港日期、是否到项目现场、到达项目现场日期、备注
interface columnVO {
  type?: string
  minWidth?: number
  field?: string
  title?: string
  fixed?: string
  slots?: any
  key: string
}

const columns = ref<columnVO[]>([
  { type: 'checkbox', key: 'checkbox', minWidth: 50, fixed: 'left' },
  { type: 'seq', key: 'seq', minWidth: 70, title: '序号', fixed: 'left' },
  {
    title: '需求清单号',
    field: 'contractNo',
    key: 'contractNo',
    minWidth: 200,
  },
  { title: '项目名称', field: 'programName', key: 'programName', minWidth: 140 },
  { title: '采购业务员', field: 'purchaser', key: 'purchaser', minWidth: 120 },
  { title: '状态', field: 'purchaseStatusLabel', key: 'purchaseStatusLabel', minWidth: 120 },
  { title: '项目编号', field: 'programNumber', key: 'programNumber', minWidth: 120 },
  { title: '供应商名称', field: 'supplier', key: 'supplier', minWidth: 120 },
  { title: '采购公司', field: 'purchaseCorp', key: 'purchaseCorp', minWidth: 180 },
  { title: '付款条件', field: 'paymentCondition', key: 'paymentCondition', minWidth: 120 },
  { title: '类别', field: 'contractCategory', key: 'contractCategory', minWidth: 120 },
  {
    title: '合同总金额',
    field: 'purchaseAmount',
    key: 'purchaseAmount',
    minWidth: 120,
  },
  {
    title: '合同交货日期',
    field: 'deliveryDate',
    key: 'deliveryDate',
    minWidth: 140,
  },
  {
    title: '采购接单日期',
    field: 'generateDate',
    key: 'generateDate',
    minWidth: 120,
  },
  {
    title: '追溯号',
    field: 'traceBackNo',
    key: 'traceBackNo',
    minWidth: 120,
  },
  {
    title: '产品信息收集表',
    field: 'productInfo',
    key: 'productInfo',
    minWidth: 200,
  },
  {
    title: '单证信息录用卡',
    field: 'documentInfo',
    key: 'documentInfo',
    minWidth: 120,
  },
  { title: '产品信息状态', field: 'goodsSubmitStatusLabel', key: 'goodsSubmitStatusLabel', minWidth: 120 },
  { title: '关务归类状态', field: 'entryClassifyStatusLabel', key: 'entryClassifyStatusLabel', minWidth: 120 },
  { title: '项目客户', field: 'programCustomer', key: 'programCustomer', minWidth: 120 },
  // { title: '项目名称', field: 'programName', key: 'programName', minWidth: 120 },
  // { title: '项目编号', field: 'programNumber', key: 'programNumber', minWidth: 120 },
  { title: '项目采购经理', field: 'purchaseManager', key: 'purchaseManager', minWidth: 120 },
  { title: '申购单号', field: 'applyBuyNo', key: 'applyBuyNo', minWidth: 120 },
  {
    title: '申购单审批完结日期',
    field: 'approvalCompletionDate',
    key: 'approvalCompletionDate',
    minWidth: 150,
  },
  { title: '商品名称', field: 'goodsName', key: 'goodsName', minWidth: 120 },
  { title: '规格/型号', field: 'goodsSpecificationModel', key: 'goodsSpecificationModel', minWidth: 120 },
  {
    title: '采购数量',
    field: 'purchaseQuantity',
    key: 'purchaseQuantity',
    minWidth: 120,
  },
  { title: '成交单位', field: 'tradeUnitLabel', key: 'tradeUnitLabel', minWidth: 120 },
  { title: '用途', field: 'purpose', key: 'purpose', minWidth: 120 },
  // { title: '采购业务员', field: 'purchaser', key: 'purchaser',minWidth: 120 },
  { title: '设备位号', field: 'equipmentItemNo', key: 'equipmentItemNo', minWidth: 120 },
  { title: '子项号', field: 'childItemNo', key: 'childItemNo', minWidth: 120 },
  { title: '申购人', field: 'applyBuyPerson', key: 'applyBuyPerson', minWidth: 120 },
  {
    title: '需求派单日期',
    field: 'demandAllotDate',
    key: 'demandAllotDate',
    minWidth: 120,
  },
  {
    title: '要求合同签订日期',
    field: 'requireSignedDate',
    key: 'requireSignedDate',
    minWidth: 140,
  },
  {
    title: '预计合同签订日期',
    field: 'planSignedDate',
    key: 'planSignedDate',
    minWidth: 140,
  },
  { title: '预算金额（万元)', field: 'budgetAmount', key: 'budgetAmount', minWidth: 130 },
  {
    title: '项目要求返资料日期',
    field: 'requireReturnDataDate',
    key: 'requireReturnDataDate',
    minWidth: 120,
  },
  {
    title: '预计返资料日期',
    field: 'planReturnDataDate',
    key: 'planReturnDataDate',
    minWidth: 120,
  },
  {
    title: '要求集港日期',
    field: 'requirePortGatherDate',
    key: 'requirePortGatherDate',
    minWidth: 120,
  },
  {
    title: '要求海外到货日期',
    field: 'requirePortArriveDate',
    key: 'requirePortArriveDate',
    minWidth: 140,
  },
])

export default function useTables() {
  return { columns }
}
