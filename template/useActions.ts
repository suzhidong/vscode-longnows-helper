import { downloadExcel } from '@longnows/lib'
import { message } from 'ant-design-vue'
import { ref, reactive } from 'vue'


export default function useAction(CommonTableRef) {
  const actionConfig = ref<any[]>([
    {
      command: 'export',
      type: 'default',
      label: '数据导出',
      disabled: false,
      need: true,
      children: [
        { label: '勾选导出', childCommand: 'check' },
        { label: '批量导出', childCommand: 'all' },
      ],
      group: '1'
    },
  ])
  const actionFun = reactive({
    export: {
      check: () => {
        let select = CommonTableRef.value.getCheckRecords()
        if (!select.length) {
          message.warning('请至少勾选一条数据')
          return
        }
        let ids = select.map((e) => e.uid)
        exportExcel(ids)
      },
      all: () => {
        exportExcel()
      },
    },
  })
  const exportExcel = (uidList?: string[]) => {
    let data = reactive({
      taskCode: 'ScmDemand',
      name: '采购合同付款台账报表',
      header: CommonTableRef.value.exportTemplate,
      searchReq: {
        ...CommonTableRef.value.searchMethods.getFieldsValue(),
        uidList,
      },
    })
    downloadExcel('/scm/api/v1/export', data, {})
  }
  return { actionConfig, actionFun }
}
