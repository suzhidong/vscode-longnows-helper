import { reactive, ref } from 'vue'
import type { FormSchema } from '@longnows/ui'
import { optionsDirtApi } from '@/api/purchaseApi'

// let searchScopeOptions = reactive([
//   {label:'启用',value:'1'},
//   {label:'禁用',value:'0'},
// ])

let purStatusOpts = ref<any>([])
// 采购公司下拉
let purchaseCorpOpts = ref<any>([])
// 项目下拉
//  let programsOptions = ref<any>([])
// 采购类别
let contractCategoryOption = ref<any>([])
// 供应商下拉
let vendorCorpOpts = ref<any>([])
// 采购业务员、需求清单号、状态、项目名称、项目编号、类别、
// 供应商名称、采购公司、付款条件、合同总金额、采购接单日期、
// 合同交货日期
export default function useForms() {
  // 采购业务员下拉
  let purchaserOptions = ref<any>([])
  const searchSchemas: FormSchema[] = reactive([
    {
      label: '采购业务员',
      field: 'purchaserId',
      component: 'Select',
      required: false,
      colProps: {
        span: 8,
      },
      componentProps: () => {
        return {
          options: purchaserOptions.value,
        }
      },
    },
    {
      label: '需求清单号',
      field: 'contractNo',
      component: 'Input',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '状态',
      field: 'purchaseStatus',
      component: 'Select',
      required: false,
      colProps: {
        span: 8,
      },
      componentProps: () => {
        return {
          options: purStatusOpts.value,
        }
      },
    },
    {
      label: '项目名称',
      field: 'programName',
      component: 'Input',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '项目编号',
      field: 'programNumber',
      component: 'Input',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '类别',
      field: 'contractCategory',
      component: 'Select',
      required: false,
      colProps: {
        span: 8,
      },
      componentProps: () => {
        return {
          options: contractCategoryOption.value,
        }
      },
    },
    {
      label: '供应商',
      field: 'supplierId',
      component: 'Select',
      required: false,
      colProps: {
        span: 8,
      },
      componentProps: () => {
        return {
          options: vendorCorpOpts.value,
        }
      },
    },
    {
      label: '采购公司',
      field: 'purchaseCorp',
      component: 'Select',
      required: false,
      colProps: {
        span: 8,
      },
      componentProps: () => {
        return {
          options: purchaseCorpOpts.value,
        }
      },
    },
    {
      label: '付款条件',
      field: 'paymentCondition',
      component: 'Input',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '合同总金额',
      field: 'contractAmount',
      component: 'Input',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '采购接单日期',
      field: 'generateDate',
      component: 'NowDatePicker',
      required: false,
      colProps: {
        span: 8,
      },
    },
    {
      label: '合同交货日期',
      field: 'deliveryDate',
      component: 'NowDatePicker',
      required: false,
      colProps: {
        span: 8,
      },
    },
  ])

  let getPurStatus = () => {
    optionsDirtApi.getDemandStatus('purchase_status').then((res) => {
      if (res.code === 0) {
        purStatusOpts.value = res.data.map((item) => ({
          label: item.paramName,
          value: item.paramCode,
        }))
      }
    })
  }
  getPurStatus()

  let getOptions = () => {
    // 获取所有项目的接口
    optionsDirtApi.getAllPrograms().then((res) => {
      if (res.code === 0) {
        programsOptions.value = res.data.map((item) => ({
          label: item.programName,
          value: item.uid,
          programNumber: item.programNumber,
          programCustomer: item.programCustomer,
        }))
      }
    })
    optionsDirtApi.getPurchaseCorp({ corpType: 8 }, { page: 1, size: 9999 }).then((res) => {
      if (res.code === 0) {
        purchaseCorpOpts.value = res.data.map((item) => ({
          label: item.corpName,
          value: item.uid,
        }))
      }
    })
    optionsDirtApi.getDictionary('contract_category').then((res) => {
      if (res.code === 0) {
        contractCategoryOption.value = res.data.map((item) => ({
          label: item.paramName,
          value: item.paramCode,
        }))
      }
    })
    optionsDirtApi.getBaseInfoAllPurchasers().then((res) => {
      if (res.code === 0) {
        purchaserOptions.value = res.data.map((item) => ({
          label: item.userName,
          value: item.uid,
        }))
      }
    })
  }

  optionsDirtApi.getPurchaseCorp({ corpType: 7 }, { page: 1, size: 9999 }).then((res) => {
    if (res.code === 0) {
      vendorCorpOpts.value = res.data.map((item) => ({
        label: item.corpName,
        value: item.uid,
      }))
    }
  })
  getOptions()

  const fieldMapToTime: any[] = [
    ['generateDate', ['generateDateStart', 'generateDateEnd'], 'YYYY-MM-DD'],
    ['deliveryDate', ['deliveryDateStart', 'deliveryDateEnd'], 'YYYY-MM-DD'],
  ]

  return {
    fieldMapToTime,
    searchSchemas,
  }
}
// 项目下拉
let programsOptions = ref<any>([])
